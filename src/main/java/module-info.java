module poe.parcialdos {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.jfoenix;
    requires AnimateFX;

    opens poe.parcialdos to javafx.fxml;
    exports poe.parcialdos;
}
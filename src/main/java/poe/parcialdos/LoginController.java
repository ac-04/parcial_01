package poe.parcialdos;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import animatefx.animation.FadeInUp;
import animatefx.animation.FadeOutDown;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RegexValidator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class LoginController implements Initializable {

    @FXML
    private AnchorPane hintText;
    @FXML
    private JFXButton btnLogIn;
    @FXML
    private JFXTextField txtUser;
    @FXML
    private JFXTextField txtPass;
    @FXML
    private void switchToHome() throws IOException {
        App.setRoot("home");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new FadeOutDown(this.btnLogIn).setSpeed(1.75).play();
        new FadeInUp(this.btnLogIn).setSpeed(0.75).setDelay(Duration.seconds(2)).play();
        new FadeInUp(this.hintText).setSpeed(0.75).setDelay(Duration.seconds(1)).play();
        this.validateInputs();
    }

    void validateInputs(){
        RegexValidator onlyStringValidator = new RegexValidator();
        onlyStringValidator.setRegexPattern("^[_A-Za-z0-9-+]+");
        onlyStringValidator.setMessage("error solo letras y numeros");

        RegexValidator validator = new RegexValidator();
        validator.setRegexPattern("^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        validator.setMessage("email no valido");

        this.txtUser.textProperty().addListener((observable, oldValue, newValue) -> txtUser.validate());
        this.txtPass.textProperty().addListener((observable, oldValue, newValue) -> txtPass.validate());
        this.txtUser.getValidators().add(validator);
        this.txtPass.getValidators().add(onlyStringValidator);
    }

    @FXML
    void login() throws IOException {
        if( txtUser.validate() && txtPass.validate() ){
            // passed
            this.switchToHome();
        }
    }
}

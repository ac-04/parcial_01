package poe.parcialdos.Clientes.Model;

import javafx.beans.property.SimpleStringProperty;

public class Cliente {

    private SimpleStringProperty dui;
    private SimpleStringProperty name;
    private SimpleStringProperty Email;
    private SimpleStringProperty birthdate;
    private SimpleStringProperty phone;

    public Cliente(String dui, String name, String email, String birthdate, String phone) {
        this.dui = new SimpleStringProperty(dui);
        this.name = new SimpleStringProperty(name);
        Email = new SimpleStringProperty(email);
        this.birthdate = new SimpleStringProperty(birthdate);
        this.phone = new SimpleStringProperty(phone);
    }

    public Cliente() {
        this("0", "", "", "", "");

    }

    public String getDui() {
        return dui.get();
    }

    public SimpleStringProperty duiProperty() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui.set(dui);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getEmail() {
        return Email.get();
    }

    public SimpleStringProperty emailProperty() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email.set(email);
    }

    public String getBirthdate() {
        return birthdate.get();
    }

    public SimpleStringProperty birthdateProperty() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate.set(birthdate);
    }

    public String getPhone() {
        return phone.get();
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }
}

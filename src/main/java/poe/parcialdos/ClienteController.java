package poe.parcialdos;

import animatefx.animation.Shake;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RegexValidator;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.util.Duration;
import poe.parcialdos.Clientes.Model.Cliente;
import poe.parcialdos.helpers.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

public class ClienteController implements Initializable {

    @FXML
    private DialogPane dialogPane;
    @FXML
    private JFXTextField dui;
    @FXML
    private JFXTextField nombre;
    @FXML
    private JFXTextField email;
    @FXML
    private JFXTextField birth;
    @FXML
    private JFXTextField phone;

    public Cliente cliente;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setCliente(Cliente cliente, ObservableList<Cliente> listClients) {

        this.cliente = cliente;
        dui.setText(cliente.getDui());
        nombre.setText(cliente.getName());
        email.setText(cliente.getEmail());
        birth.setText(cliente.getBirthdate());
        phone.setText(cliente.getPhone());

        if (dui.getText().equals("0")) {
            dui.clear();
            dui.setDisable(false);
        }else{
            dui.setDisable(true);
        }
        Button okButton = (Button) dialogPane.lookupButton(ButtonType.OK);
        okButton.addEventFilter(ActionEvent.ACTION, event
                        -> {

            this.setNombreValidator();
            this.setDuiValidator();
            this.setEmailValidator();
            this.setBirthValidator();
            this.setPhoneValidator();

            System.out.println("dui:");
            System.out.println(this.dui.validate());
            if ( !this.dui.validate() && !this.nombre.validate() && !this.email.validate() && !this.birth.validate() && !this.phone.validate()) {
               this.moveFields();
                event.consume();
                    } else {
                        try {
                            cliente.setDui( dui.getText() );
                            cliente.setName( nombre.getText() );
                            cliente.setEmail( email.getText() );
                            cliente.setBirthdate( birth.getText() );
                            cliente.setPhone( phone.getText() );
                            if (listClients.contains(cliente) && !dui.isDisable()) {

                                Dialogs.showErrorDialog("Error", null, "Ya existe un cliente con dui " + cliente.getDui());
                                event.consume();
                            }
                        } catch (NumberFormatException e) {
                            Dialogs.showErrorDialog("Error", null, "Todos los datos son requeridos");
                            event.consume();
                        }
                    }
                }
        );
    }


    void moveFields(){
        if(!this.dui.validate()){
            new Shake(this.dui).play();
        }
        if(!this.nombre.validate()){
            new Shake(this.nombre).play();
        }
        if(!this.birth.validate()){
            new Shake(this.dui).play();
        }
        if(!this.email.validate()){
            new Shake(this.email).play();
        }
        if(!this.birth.validate()){
            new Shake(this.birth).play();
        }
        if(!this.phone.validate()){
            new Shake(this.phone).play();
        }
    }

    void setNombreValidator(){
        RegexValidator onlyStringValidator = new RegexValidator();
        onlyStringValidator.setRegexPattern("^[_A-Za-z]+");
        onlyStringValidator.setMessage("error solo letras");
        this.nombre.textProperty().addListener((observable, oldValue, newValue) -> nombre.validate() );
        this.nombre.getValidators().add(onlyStringValidator);
    }

    void setDuiValidator(){
        RegexValidator ValidatorDui = new RegexValidator();
        ValidatorDui.setRegexPattern("^[0-9]{8}-[0-9]{1}");
        ValidatorDui.setMessage("dui = 8 digitos guion 1 digito");
        this.dui.textProperty().addListener((observable, oldValue, newValue) -> dui.validate());
        this.dui.getValidators().add(ValidatorDui);
    }

    void setEmailValidator(){
        RegexValidator validator = new RegexValidator();
        validator.setRegexPattern("^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        validator.setMessage("email no valido");
        this.email.textProperty().addListener((observable, oldValue, newValue) -> email.validate());
        this.email.getValidators().add(validator);
    }

    void setBirthValidator(){
        RegexValidator validator = new RegexValidator();
        validator.setRegexPattern("^\\d{2}-\\d{2}-\\d{4}$");
        validator.setMessage("fecha en patron DD-MM-YYYY");
        this.birth.textProperty().addListener((observable, oldValue, newValue) -> birth.validate());
        this.birth.getValidators().add(validator);
    }

    void setPhoneValidator(){
        RegexValidator validator = new RegexValidator();
        validator.setRegexPattern("^[0-9]{4}-[0-9]{4}");
        validator.setMessage("numero de telefono en patron 0000-0000");
        this.phone.textProperty().addListener((observable, oldValue, newValue) -> phone.validate());
        this.phone.getValidators().add(validator);
    }
}

package poe.parcialdos;


import animatefx.animation.FadeInLeft;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Duration;
import poe.parcialdos.Clientes.Model.Cliente;
import poe.parcialdos.Clientes.Repository.ClientsRepository;
import poe.parcialdos.helpers.Dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class ClientesController  implements Initializable {

    enum DialogMode {
        ADD, UPDATE
    }

    @FXML
    private TableView<Cliente> TableClientes;
    @FXML
    private TableColumn<Cliente,String> clDui;
    @FXML
    private TableColumn<Cliente,String> clNombre;
    @FXML
    private TableColumn<Cliente,String> clEmail;
    @FXML
    private TableColumn<Cliente,String> clBirth;
    @FXML
    private TableColumn<Cliente,String> clPhone;


    @FXML
    private Label h1;
    @FXML
    private Label h2;
    @FXML
    private Label h3;

    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private  JFXButton btnEdit;
    @FXML
    private  JFXButton btnDel;

    private ObservableList<Cliente> listClientes = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.SetTable();
        new FadeInLeft(this.h1).setDelay(Duration.seconds(1)).play();
        new FadeInLeft(this.h2).setDelay(Duration.seconds(2)).play();
        new FadeInLeft(this.h3).setDelay(Duration.seconds(3)).play();
    }

    void SetTable(){

        this.clDui.setCellValueFactory((dui)->dui.getValue().duiProperty());
        this.clNombre.setCellValueFactory((nombre)->nombre.getValue().nameProperty());
        this.clEmail.setCellValueFactory((email)->email.getValue().emailProperty());
        this.clBirth.setCellValueFactory((birth)->birth.getValue().birthdateProperty());
        this.clPhone.setCellValueFactory((phone)->phone.getValue().phoneProperty());

        listClientes = FXCollections.observableArrayList(ClientsRepository.getListClients() );
        System.out.println(listClientes.get(0).getName());
        this.TableClientes.setItems(listClientes);

    }

    @FXML
    private void switchLogin() throws IOException {
        App.setRoot("login");
    }

    @FXML
    private void agregar(ActionEvent event){

        Cliente cliente = null;
        String dialogTitle = "";
        DialogMode mode;

        System.out.println(event.getSource().equals(btnEdit));
        if (event.getSource().equals(btnEdit)) {
            mode = DialogMode.UPDATE;
            dialogTitle = "Actualizar Cliente";
            cliente = TableClientes.getSelectionModel().getSelectedItem();
        } else if (event.getSource().equals(btnAdd)) {
            mode = DialogMode.ADD;
            dialogTitle = "Nuevo Cliente";
            cliente = new Cliente();
        } else {
            return;
        }

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/poe/parcialdos/cliente.fxml"));
            DialogPane libroDialogPane = fxmlLoader.load();

            ClienteController clienteControllador = fxmlLoader.getController();

            clienteControllador.setCliente(cliente,listClientes);

            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setDialogPane(libroDialogPane);
            dialog.setTitle(dialogTitle);
            Optional<ButtonType> clickedButton = dialog.showAndWait();
            if (clickedButton.get() == ButtonType.OK) {
                if (mode == DialogMode.ADD) {
                    listClientes.add(cliente);
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void Eliminar(ActionEvent event) {
        Cliente libro = TableClientes.getSelectionModel().getSelectedItem();
        Optional<ButtonType> result
                = Dialogs.showConfirmationDialog("Confirmación", null,
                "¿Desea eliminar el cliente con dui " + libro.getDui() + "?");
        if (result.get() == ButtonType.YES) {
            listClientes.remove(libro);
            TableClientes.getSelectionModel().select(null);
        }
    }

    @FXML
    private void editar(ActionEvent event) {
        this.agregar(event);
    }
}
